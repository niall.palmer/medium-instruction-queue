import instruction.messages.InstructionMessage;
import instruction.messages.InstructionQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class InstructionMessageTest {
    
    InstructionMessage instructionMessage1;
    InstructionMessage instructionMessage2;
    InstructionMessage instructionMessage3;
    InstructionMessage instructionMessage4;
    InstructionMessage instructionMessage5;
    InstructionMessage instructionMessage6;
    InstructionMessage instructionMessage7;
    InstructionMessage instructionMessage8;
    InstructionMessage instructionMessage9;
    InstructionMessage instructionMessage10;
    InstructionQueue queue;
    String dateTime;
    DateTimeFormatter dateTimeFormatter;


    @BeforeEach
    void setUp() {
        dateTime = "2023-03-29T10:01:02.012";
        dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
    }
    
    @Test
    void instructionMessageConstructorAndGettersGetFieldsCorrectly() {

        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        assertEquals("InstructionMessage", instructionMessage1.getInstructionMessageStr());
        assertEquals('A', instructionMessage1.getInstructionType());
        assertEquals("AB12", instructionMessage1.getProductCode());
        assertEquals(50, instructionMessage1.getQuantity());
        assertEquals(123, instructionMessage1.getUom());
        assertEquals("2023-03-29T10:01:02.012", instructionMessage1.getTimestamp());
    }
    
    @Test
    void instructionMessageGoesOntoInstructionQueue() {
        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        queue = new InstructionQueue();
        queue.enqueue(instructionMessage1);
        assertEquals( instructionMessage1, queue.getFirstInstructionMessage());
    }
    
    @Test
    void enqueueThrowsExceptionsWhenMessageInvalid() {
        queue = new InstructionQueue();
        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage2 = new InstructionMessage("InsturctionMesage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage3 = new InstructionMessage("InstructionMessage", 'H', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage4 = new InstructionMessage("InstructionMessage", 'A', "G4J1", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage5 = new InstructionMessage("InstructionMessage", 'A', "AB12", 0, 123, "2023-03-29T10:01:02.012");
        instructionMessage6 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 431, "2023-03-29T10:01:02.012");
        instructionMessage7 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29X10:01:02.012");
        instructionMessage8 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "1942-03-29T10:01:02.012");
        instructionMessage9 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2056-03-29T10:01:02.012");
        instructionMessage10 = new InstructionMessage(".InstructionMessage", 'A', "AB12", 50, 123, "2023-14-47T10:01:02.012");
        queue.enqueue(instructionMessage1);

        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage2);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage3);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage4);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage5);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage6);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage7);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage8);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage9);
                }),
                () -> assertThrows(IllegalArgumentException.class, () -> {
                    queue.enqueue(instructionMessage10);
                })
        );
        
    }
    
    @Test
    void receiveReceivesMessageAndQueuesItSuccessfully() {
        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        queue = new InstructionQueue();
        queue.receive("InstructionMessage A AB12 50 123 2023-03-29T10:01:02.012");
        
        assertEquals(instructionMessage1.toString(), queue.getFirstInstructionMessage().toString());
    }
    
    @Test
    void receiveThrowsExceptionWhenMessageNotInCorrectFormat() {
        queue = new InstructionQueue();
        assertThrows(IllegalArgumentException.class, () -> {queue.receive("InstructionMessage A AB12 50 123");});
        assertThrows(IllegalArgumentException.class, () -> {queue.receive("InstructionMessage A AB12 50 123 2023-03-29T10:01:02.012 Foo");});
    }
    
    @Test
    void showShowsInstructionMessagesInQueue() {
        queue = new InstructionQueue();
        
    }
    
    @Test
    void countCountsNumberOfInstructionMessages() {
        queue = new InstructionQueue();
        
        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage2 = new InstructionMessage("InstructionMessage", 'B', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        instructionMessage3 = new InstructionMessage("InstructionMessage", 'C', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        
        queue.enqueue(instructionMessage1);
        queue.enqueue(instructionMessage2);
        queue.enqueue(instructionMessage3);
        
        assertEquals(3, queue.count());
    }
    
    @Test
    void isEmptyCorrectlyShowsIfQueueIsEmpty() {
        queue = new InstructionQueue();
        assertTrue(queue.isEmpty());
    
        instructionMessage1 = new InstructionMessage("InstructionMessage", 'A', "AB12", 50, 123, "2023-03-29T10:01:02.012");
        queue.enqueue(instructionMessage1);
        assertFalse(queue.isEmpty());
    }
}