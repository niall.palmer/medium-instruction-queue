package instruction.messages;

public interface MessageReceiver {
    void receive(String message);
}
