package instruction.messages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class InstructionMessageValidityCheckers {

    public static boolean isValidInstructionMessageStr(String instructionMessageStr) {
        return instructionMessageStr.equals(InstructionMessage.INSTRUCTION_MESSAGE);
    }

    public static boolean isValidInstructionType(char instructionType) {
        return (instructionType == 'A' || instructionType == 'B' || instructionType == 'C' || instructionType == 'D');
    }

    public static boolean isValidProductCode(String productCode) {
        return
                productCode.length() == 4
                && Character.isUpperCase(productCode.charAt(0))
                && Character.isUpperCase(productCode.charAt(1))
                && Character.isDigit(productCode.charAt(2))
                && Character.isDigit(productCode.charAt(3));
    }

    public static boolean isValidQuantity(int quantity) {
        return quantity > 0;
    }

    public static boolean isValidUom(int uom) {
        return uom >= 0 && uom < 256;
    }

    public static boolean isValidTimestamp(String timestamp) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            LocalDateTime.parse(timestamp, dateTimeFormatter);
        } catch (DateTimeParseException exception){
            return false;
        }
        return LocalDateTime.parse(timestamp).getYear() >= 1970 && LocalDateTime.parse(timestamp).getYear() <= LocalDateTime.now().getYear();
    }

}
