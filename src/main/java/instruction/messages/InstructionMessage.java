package instruction.messages;

public class InstructionMessage {
    public static final String INSTRUCTION_MESSAGE = "InstructionMessage";
    private String instructionMessageStr;
    private final char instructionType;
    private final String productCode;
    private final int quantity;
    private final int uom;
    private final String timestamp;

    public InstructionMessage(String instructionMessageStr, char instructionType, String productCode, int quantity, int uom, String timestamp) {
        this.instructionMessageStr = instructionMessageStr;
        this.instructionType = instructionType;
        this.productCode = productCode;
        this.quantity = quantity;
        this.uom = uom;
        this.timestamp = timestamp;
    }

    public String getInstructionMessageStr() {
        return instructionMessageStr;
    }

    public char getInstructionType() {
        return instructionType;
    }

    public String getProductCode() {
        return productCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getUom() {
        return uom;
    }

    public String getTimestamp() {
        return timestamp;
    }
    
    @Override
    public String toString() {
        return instructionMessageStr + " " + instructionType + " " + productCode + " " + quantity + " " + uom + " " + timestamp;
    }
}
