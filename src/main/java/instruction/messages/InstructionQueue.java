package instruction.messages;

import java.util.ArrayList;
import java.util.List;
import static instruction.messages.InstructionMessageValidityCheckers.*;


public class InstructionQueue implements MessageReceiver {
    private final List<InstructionMessage> instructionQueue = new ArrayList<>();
    
    public void enqueue(InstructionMessage instructionMessage) {
        if (!isValidInstructionMessageStr(instructionMessage.getInstructionMessageStr()))
            throw new IllegalArgumentException("The instruction message must begin with instruction.messages.InstructionMessage");
        
        if (!isValidInstructionType(instructionMessage.getInstructionType()))
            throw new IllegalArgumentException("The instruction type must be one of A, B, C, D");
        
        if (!isValidProductCode(instructionMessage.getProductCode()))
            throw new IllegalArgumentException("The product code must be two uppercase letters followed by two digits");
        
        if (!isValidQuantity(instructionMessage.getQuantity()))
            throw new IllegalArgumentException("The quantity must be at least 1");
        
        if (!isValidUom(instructionMessage.getUom()))
            throw new IllegalArgumentException("The UOM must be between 0 and 255 inclusive");
        
        if (!isValidTimestamp(instructionMessage.getTimestamp()))
            throw new IllegalArgumentException("The timestamp must be a valid date in the format of yyyy-MM-dd'T'HH:mm:ss.SSS, after 1/1/1970, and before now");

        instructionQueue.add(instructionMessage);
    }
    
    public InstructionMessage getFirstInstructionMessage() {
        return instructionQueue.get(0);
    }
    
    public List<InstructionMessage> show() {
        return instructionQueue;
    }
    
    public int count() {
        return instructionQueue.size();
    }
    
    public boolean isEmpty() {
        return instructionQueue.size() == 0;
    }
    
    @Override
    public void receive(String message) {
        String[] splitMessage = message.split(" ");
        
        if (splitMessage.length != 6)
            throw new IllegalArgumentException("There are an incorrect number of arguments to receive an instruction message");
        
        enqueue(new InstructionMessage(
                splitMessage[0],
                splitMessage[1].charAt(0),
                splitMessage[2],
                Integer.parseInt(splitMessage[3]),
                Integer.parseInt(splitMessage[4]),
                splitMessage[5]));
    }
}
